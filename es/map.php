<?php
/**
 * @package WordPress
 * @subpackage Provis
 * @since 3.0.0
 */

/* Template Name: Map */

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'map.php' == basename($_SERVER['SCRIPT_FILENAME'])) die('Por favor, no cargue este archivo directamente.');

get_header(); ?>
<div id="wrapper">
	<div id="left-sidebar">
		<?php
		if (function_exists('novo_services')) novo_services();
		if ($post->post_parent):
			$section_pages = get_pages('child_of=' . $post->post_parent . '&sort_column=menu_order');
			if ($section_pages):
				$zone = get_page($post->post_parent);
		?>
		<div id="section-menu">
			<h2 class="<?php echo $zone->post_name; ?>"><?php echo $zone->post_title; ?></h2>
			<?php foreach ($section_pages as $page) { ?>
				<a<?php if (is_page($page->post_name)): ?> class="curr"<?php endif; ?> href="<?php echo get_page_link($page->ID); ?>" rel="section"><?php echo $page->post_title; ?></a>
			<?php } ?>
		</div>
		<?php endif; endif; ?>
	</div>
	<div id="content">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div id="content-title">
			<img src="<?php bloginfo('template_directory'); ?>/images/icons/content_default.jpg" width="56" height="56" alt="" />
			<h2><?php the_title(); ?></h2>
			<?php $subtitle = get_post_meta($post->ID, 'subtitle', true);
			if ($subtitle): ?><h3><?php echo $subtitle; ?></h3><?php endif; ?>
		</div>
		<div id="content-wrap">
			<?php the_content(); ?>
			<p><strong>Para visualizar este mapa requiere tener instalado Adobe Flash Player 9 o superior.</strong> <a href="http://get.adobe.com/es/flashplayer/" target="_blank">Actualizar Flash Player</a></p>
		</div>
		<div id="content-nav">
			<?php if ($post->post_parent > 0): ?>
			<div class="previous"><?php if (function_exists('previous_page_not_post')) echo previous_page_not_post(); ?></div>
			<div class="next"><?php if (function_exists('next_page_not_post')) echo next_page_not_post(); ?></div>
			<?php endif; ?>
		</div>
		<?php endwhile; endif; ?>
	</div>
	<div id="right-sidebar">
		<?php if (function_exists('novo_banners')) novo_banners(2); ?>
	</div>
	<?php $zone_id = $post->post_parent ? $post->post_parent : $post->ID;
	if (function_exists('novo_tidbits')) novo_tidbits($zone_id); ?>
</div>
<?php get_footer(); ?>

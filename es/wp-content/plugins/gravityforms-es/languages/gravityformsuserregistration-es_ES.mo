��    D      <  a   \      �     �  �   �     p     w          �     �     �     �     �  
   �     �     �     �               $     +     K     [     z     �  
   �     �     �     �     �     �  	   �     �  A   �     /     8     A     W     n     s     x     �  [   �     �     �     	     	  
   +	  
   6	  "   A	     d	  K   �	  (   �	  #   �	  "   
     >
  <   E
     �
     �
     �
     �
     �
  O   �
     8  %   N     t     �     �     �     �  G  �     �  �   �     �     �     �     �     �     �  	   �  !   �  
          &   $     K     c     j  	   {  +   �     �  +   �     �     
       
   &     1     :     M  	   \  	   f     p  O   �     �  	   �     �     	          "     *     A  o   X     �  #   �     �          ,     H  ,   Z      �  P   �  ,   �  "   &  $   I  
   n  D   y     �     �  '   �            W   1     �  ,   �     �     �     �     �     �     *   5   #               7              &         	                 ?      8             %      D   "       -                >      
         @   1          <   '   9       3   A   !         .                ,   $                   ;              4   0   (   =   C                             6           +   2   :   B           )                /           %s site %sRegister%s your copy of Gravity Forms to receive access to automatic upgrades and support. Need a license key? %sPurchase one now%s. Active Add New Apply Auto Generate Password Bulk action Cancel Create Site Custom Registration Page Deactivate Delete Delete selected feeds?  Delete this feed?  Edit Email Address Enable Enable Custom Registration Page Enter Meta Name Feed Updated. %sback to list%s Feed deleted. Feeds deleted. First Name Form Full Gravity Form Gravity Forms  Inactive Last Name Multisite Options Oops!! Something went wrong.%sPlease try again or %scontact us%s. Password Register Register the user if  Registration Condition Role Save Save Settings Select a form Select the Gravity Form you would like to use to register users for your WordPress website. Send Email? Set As Post Author Set User as %s Site Address Site Admin Site Title The email address can not be empty The username can not be empty There is a new version of Gravity Forms User Registration Add-On available. This email address is already registered This username is already registered Uninstall User Registration Add-On Update Update <strong>site</strong> when subscription is cancelled. User Registration User Registration Forms User Registration Settings Username View version %s Details You don't have any User Registration feeds configured. Let's go %screate one%s! [%s] New Site Created \'Cancel\' to stop, \'OK\' to delete. add another rule is is not remove this rule required Project-Id-Version: Gravity Forms User Registration
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-07-16 05:47-0500
PO-Revision-Date: 2016-06-19 19:45+0200
Last-Translator: David <david@closemarketing.es>
Language-Team: Closemarketing <david@closemarketing.es>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: _;gettext;gettext_noop;_e;__
X-Poedit-Basepath: .
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: C:/xampp/htdocs/wp-content/plugins/gravityformsuserregistration
 Sitio %s %sRegistra%s tu copia de Gravity Forms para recibir acceso automático a actualizaciones y soporte. ¿Necesitas una clave de licencia? %sCompra Ahora una%s. Activar Añadir Nuevo Aplicar Auto Generar Contraseña Acción masiva Cancelar Crear Web Página de Registro Personalizada Desactivar Borrar ¿Borrar los conectores seleccionados? ¿Borrar este conector? Editar Dirección Email Habilitar Habilitar Página de Registro Personalizada Introducir Nombre del Meta Conector Actualizado. %svolver a la lista%s Conector borrado. Conectores borrados. Nombre Formulario Completo Formulario Gravity Gravity Forms  Inactivar Apellidos Opciones Multisitio Oops!! Algo fue mal. %sPor favor intenta otra vez o %scontacta con nosotros %s. Contraseña Registrar Registrar si el usuario cumple Condicionar Registro Rol Guardar Guardar Configuración Seleccionar Formulario ﻿Selecciona el formulario Gravity que desea utilizar para registrar a los usuarios de tu sitio web WordPress. ¿Enviar Email? ﻿Establecer como Autor de Entrada Establecer Usuario como %s Dirección de la Web Administrador del Sitio Web Título de la Web La dirección de email no puede estar vacía El Usuario no puede estar vacío Hay una nueva versión disponible de este Addon Gravity Forms User Registration. Esta dirección de email está ya registrada Este usuario ha sido ya registrado Desinstalar Add-On User Registration Actualizar Actualizar <strong>sitio</strong> cuando la suscripción se cancela. Registro Usuarios User Registration Forms Configuración del Registro de usuarios Usuario Ver Detalles versión %s No tienes ningún conector de registro de usuario configurado. ¡Vamos %sa crear uno%s! [%s] Nuevo Sitio Creado \'Cancelar\' para parar, \'OK\' para borrar. añadir otra regla es no es borra esta regla obligatorio 
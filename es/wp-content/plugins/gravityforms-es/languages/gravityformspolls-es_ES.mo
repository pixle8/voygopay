��    T      �  q   \            !     '     .     6     =     F     N  R   i     �      �     �  
   �     �                    '     <     K     X     l     �     �     �     �     �  (   �     	  	   5	     ?	     K	     T	  	   ]	     g	     t	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  	   
     
      
     .
     @
     [
     _
     m
     �
     �
     �
     �
     �
     �
     �
     �
  
   �
  X   �
  _   W  <   �     �                #     /     I  7   Z     �     �     �     �     �     �     �     �  �       �     �          	               #  V   ?     �  -   �     �     �     �     �                &     G     Z     q  /   �     �      �  #   �          ,  (   @  *   i     �     �  
   �     �  	   �     �  
   �     �          
                    "     A     M     V     k     �     �     �  	   �     �     �     �          "  ,   4  
   a     l     ~     �     �     �     �     �     �  v   �  \   t  \   �     .     @     T     n     �     �  1   �     �     �     �               5     D  2   W     @   C   P   Q          9       	      G   J   M      :   ?   L                     5      R   *           F                 K   ;           
   "   4       8   .   2   $           /   &           I       +              =      7   -      <          1       '       ,      D   S   3         6              !            O                       >   %   A          E      H                        )   N   (   #   0   B   T    1 day 1 hour 1 month 1 week 12 hours 6 hours <h2>Original Response</h2> Add a link to the form which allows the visitor to see the results without voting. Back to the poll Block repeat voting using cookie Blue Checkboxes Cookie Counts Date Disable script output Display Confirmation Display Counts Display Mode Display Percentages Display Results After Voting Display counts Display form confirmation Display form description Display form title Display percentages Display results of submitted poll fields Don't block repeat voting Drop Down Enable AJAX Entry Id Expires: Expires:  First Choice Form Gravity Forms Polls Add-On Green Mode Never Orange Other Override form settings Percentages Poll Poll Entries Poll Question Poll Results Poll Settings Poll Type Polls Radio Buttons Randomize Choices Randomize order of choices Red Repeat Voters Repeat voting is not allowed Results Results Link Results Settings Results link Results only Rocketgenius Second Choice Select a Form Select one Select this option to display the form confirmation message after the visitor has voted. Select this option to display the results of submitted poll fields after the form is submitted. Select this to display the results of submitted poll fields. Show Counts Show Percentages Show Results Link Show counts Show link to view results Show percentages Show the percentage of the total votes for each choice. Style Tab Index Start Third Choice Title Use form settings View results advanced options http://www.rocketgenius.com Project-Id-Version: Gravity Forms Spanish
Report-Msgid-Bugs-To: http://www.gravtiyhelp.com
POT-Creation-Date: 2016-06-19 19:26+0200
PO-Revision-Date: 2016-06-19 19:28+0200
Last-Translator: David <david@closemarketing.es>
Language-Team: Spanish (http://www.transifex.com/closemarketing/gravity-forms-spanish/language/es/)
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.8
 1 día 1 hora 1 mes 1 semana 12 horas 6 horas <h2>Respuesta original</h2> Añadir un enlace al formulario que permite al visitante ver los resultados sin votar. Volver para la encuesta Bloqueo votación repetida utilizando cookies Azul Casillas Verificación Cookie Conteos Fecha Desactivar salida Script Mostrar formulario confirmación Mostrar Contadores Modo de visualización Mostrar porcentajes Mostrar los resultados después de la votación Mostrar Contadores Mostrar formulario confirmación Mostrar Descripción del Formulario Mostrar Título del Formulario Mostrar Porcentajes Ver resultados enviados de esta encuesta No bloquear la repetición de la votación Desplegable Habilitar AJAX ID Entrada Expira: Finaliza: Primera Elección Formulario Gravity Forms Polls Add-On Verde Modo Nunca Naranja Otro Actualizar opciones Formulario Porcentajes Encuesta Resultados Encuestas Pregunta de la encuesta Resultados Encuestas Configuración Encuestas Tipo de encuesta Encuestas Botones de Radio Orden aleatorio de las opciones Orden aleatorio de las opciones Rojo Repetición Votos No se permite la repetición de la votación Resultados Enlace resultados Ajustes de resultados Enlace resultados Resultados sólo Rocketgenius Segunda Elección Seleccionar Formulario Seleccionar uno Selecciona esta opción para mostrar el mensaje de confirmación de formulario después de que el visitante ha votado. Seleccione esta opción para mostrar los resultados de los campos de la encuesta presentada. Seleccione esta opción para mostrar los resultados de los campos de la encuesta presentada. Mostrar recuentos Mostrar Porcentajes Mostrar Enlace Resultados Mostrar Contadores Mostrar Enlace Resultados Mostrar Percentajes Muestra el porcentaje de votos para cada opción. Estilo Ficha Índice Inicio Tercera Elección Título Configuraciones de formulario Ver resultados Opciones avanzadas https://www.closemarketing.es/likes/gravity-forms/ 
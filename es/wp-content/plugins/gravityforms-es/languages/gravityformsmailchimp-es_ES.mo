��          �      l      �     �     �     �               %     D  /   K     {     ~     �  
   �     �     �     �     �     �               :    G     [     n     v     �     �     �     �  -   �       '        .     D     S     Z     c     x     �     �     �     �                                                                	            
                      %s Settings Always Assign to group: Conditional Logic Double Opt-In Gravity Forms MailChimp Add-On Groups I understand this change, dismiss this message! If List not found in MailChimp MailChimp Add-On v3.0 Map Fields Name Options Please wait... Select a MailChimp List Send Welcome Email http://www.gravityforms.com http://www.rocketgenius.com rocketgenius Project-Id-Version: Gravity Forms MailChimp Add-On 3.7.1
Report-Msgid-Bugs-To: http://www.gravtiyhelp.com
POT-Creation-Date: 2016-06-19 19:21+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2016-06-19 19:23+0200
Language-Team: Rocketgenius <customerservice@rocketgenius.com>
X-Generator: Poedit 1.8.8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
Project-Id-Version: gravityformsmailchimp
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
Last-Translator: David <david@closemarketing.es>
Language: es_ES
X-Poedit-SearchPath-0: .
 Configuración %s  Siempre Asignar a un Grupo: Lógica condicional Doble Condición Gravity Forms MailChimp Add-On Grupos Entiendo este cambio, descartar este mensaje! Si Lista que no se encuentran en MailChimp MailChimp Add-On v3.0 Mapa de Campos Nombre Opciones Espera un momento... Seleccionar Lista MailChimp Enviar Email Bienvenida http://www.gravityforms.com http://www.rocketgenius.com rocketgenius 
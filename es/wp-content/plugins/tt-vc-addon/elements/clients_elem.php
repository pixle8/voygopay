<?php
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_tek_clients extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_tek_clients_single extends WPBakeryShortCode {
    }
}
if (!class_exists('tek_clients')) {
    class tek_clients extends TT_ADDON_CLASS
    {
        function __construct() {
            add_action('admin_init', array($this, 'tt_clients_init'));
            add_shortcode('tek_clients', array($this, 'tt_clients_container'));
            add_shortcode('tek_clients_single', array($this, 'tt_clients_single'));
        }
        // Element configuration in admin   
        function tt_clients_init() {
            // Container element configuration
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Clients", 'tt_addon_lang'),
                    "description" => __("Clients carousel with images.", 'tt_addon_lang'),
                    "base" => "tek_clients",
                    "class" => "",
                    "show_settings_on_create" => true,
                    "content_element" => true,
                    "as_parent" => array('only' => 'tek_clients_single'),
                    "icon" => plugins_url('assets/element_icons/clients.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "js_view" => 'VcColumnView'
                ));
                // Shortcode configuration
                vc_map(array(
                    "name" => __("Client", "tt_addon_lang"),
                    "base" => "tek_clients_single",
                    "content_element" => true,
                    "as_child" => array('only' => 'tek_clients'),
                    "icon" => plugins_url('assets/element_icons/clients.png', dirname(__FILE__)),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Client URL:", "tt_addon_lang"),
                            "param_name" => "client_link",
                            "description" => __("Client URL ( with http:// )")
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __("Client image:", "tt_addon_lang"),
                            "param_name" => "client_image",
                            "description" => __("Upload Client image")
                        )
                    )
                ));
            }
        }
        
        public function tt_clients_container($atts, $content = null) {
            extract(shortcode_atts(array(), $atts));
            $output = "
            <div class='slider clients'>" . do_shortcode($content) . "</div>";
            return $output;
        }
        
        public function tt_clients_single($atts, $content = null) {
            extract(shortcode_atts(array(
                'client_link'          => '',
                'client_image'          => ''
            ), $atts));
            
            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $client_image,
                'thumb_size' => 'full',
                'class' => ""
            ));
            
            $output = "<div class='clients-content'>
                        <a href='{$client_link}' target='_blank'>{$image['thumbnail']}</a>
                      </div>";
            return $output;
        }
    }
}
if (class_exists('tek_clients')) {
    $tek_clients = new tek_clients;
}
?>
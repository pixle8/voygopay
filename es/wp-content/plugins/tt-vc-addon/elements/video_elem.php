<?php
if (!class_exists('TT_ELEM_VIDEO')) {
    class TT_ELEM_VIDEO extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_video_init'));
            add_shortcode('tek_video', array($this, 'tt_video_shrt'));
        }
        // Element configuration in admin
        function tt_video_init() {
            global $icons;
            include(plugin_dir_path(dirname(__FILE__)) . 'config/icon-selector.php');
      
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Video Modal", 'tt_addon_lang'),
                    "description" => __("Video modal", 'tt_addon_lang'),
                    "base" => "tek_video",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/video.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(

                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Youtube URL", 'tt_addon_lang'),
                            "param_name" => "video_url",
                            "value" => "",
							"description" => __("Copy youtube url here", 'tt_addon_lang')
                        ),      
                        array(
                            "type" => "attach_image",
                            "heading" => __("Video preview image:", "tt_addon_lang"),
                            "param_name" => "video_image",
                            "description" => __("Upload Video preview image")
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Facebook Social URL", 'tt_addon_lang'),
                            "param_name" => "facebook_url",
                            "value" => "",
                            "description" => __("Facebook Social URL", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Twitter Social URL", 'tt_addon_lang'),
                            "param_name" => "twitter_url",
                            "value" => "",
                            "description" => __("Copy youtube url here", 'tt_addon_lang')
                        ), 
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Linkedin Social URL", 'tt_addon_lang'),
                            "param_name" => "linkedin_url",
                            "value" => "",
                            "description" => __("Linkedin Social URL", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Youtube Social URL", 'tt_addon_lang'),
                            "param_name" => "youtube_url",
                            "value" => "",
                            "description" => __("Youtube Social URL", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Google Social URL", 'tt_addon_lang'),
                            "param_name" => "google_url",
                            "value" => "",
                            "description" => __("Google social URL", 'tt_addon_lang')
                        ), 
                        array(
                            "type" => "textfield",
                            "class" => "",                           
                            "heading" => __("Pinterest Social URL", 'tt_addon_lang'),
                            "param_name" => "pinterest_url",
                            "value" => "",
                            "description" => __("Pinterest Social URL", 'tt_addon_lang')
                        )
                    )
                ));
            }
        }
		
		// Render the element on front-end
        public function tt_video_shrt($atts, $content = null)
        {
            extract(shortcode_atts(array(
                'video_url' 			=> '',
                'video_image' 			=> '',
                'facebook_url'             => '',
                'twitter_url'             => '',
                'youtube_url'             => '',
                'linkedin_url'             => '',
                'pinterest_url'             => '',
                'google_url'             => '',
            ), $atts));

            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $video_image,
                'thumb_size' => 'full',
                'class' => ""
            ));

            $output = "<div class='video-container'>                  
                        <a data-toggle='modal' data-target='#video-modal' data-backdrop='true'>
                        {$image['thumbnail']}
                        <span class='play-video'><span class='fa fa-play'></span></span></a>
                        </div>
                        <div class='video-socials'>";
            if ($facebook_url != NULL)  $output .="<a href='{$facebook_url}' target='_blank'><span class='fa fa-facebook'></span></a>";
            if ($twitter_url != NULL)  $output .="<a href='{$twitter_url}' target='_blank'><span class='fa fa-twitter'></span></a>";
            if ($youtube_url != NULL)  $output .="<a href='{$youtube_url}' target='_blank'><span class='fa fa-youtube-play'></span></a>";
            if ($google_url != NULL)  $output .="<a href='{$google_url}' target='_blank'><span class='fa fa-google-plus'></span></a>";
            if ($pinterest_url != NULL)  $output .="<a href='{$pinterest_url}' target='_blank'><span class='fa fa-pinterest'></span></a>";
            if ($linkedin_url != NULL)  $output .="<a href='{$linkedin_url}' target='_blank'><span class='fa fa-linkedin'></span></a>";
             $output .="</div>
                        <div class='modal fade video-modal' id='video-modal' role='dialog'>
                        <div class='modal-content'>
                        <div class='row'>
                        <iframe width='712' height='400' src='{$video_url}' allowfullscreen></iframe>
                        </div>
                        </div>
                        </div>";
            return $output;
        }
    }
}
if (class_exists('TT_ELEM_VIDEO')) {
    $TT_ELEM_VIDEO = new TT_ELEM_VIDEO;
}
?>
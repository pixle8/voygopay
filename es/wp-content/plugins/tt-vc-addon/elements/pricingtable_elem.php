<?php
if (!class_exists('TT_ELEM_PRICING_TABLE')) {
    class TT_ELEM_PRICING_TABLE extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_pricingtable_init'));
            add_shortcode('tek_pricing', array($this, 'tt_pricingtable_shrt'));
        }
        // Element configuration in admin	
        function tt_pricingtable_init()
        {
            global $icons;
            include(plugin_dir_path(dirname(__FILE__)) . 'config/icon-selector.php');
			
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Pricing Table", 'tt_addon_lang'),
                    "description" => __("Pricing table with extended settings.", 'tt_addon_lang'),
                    "base" => "tek_pricing",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/pricing.png', dirname(__FILE__)),
                    "category" => __('KeyDesign Elements', 'tt_addon_lang'),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "holder" => "pricingtable_title",
                            "class" => "",
                            "heading" => __("Pricing plan title:", 'tt_addon_lang'),
                            "param_name" => "pricing_title",
                            "value" => "",
                            "description" => __("Enter your pricing plan title.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Pricing plan value:", 'tt_addon_lang'),
                            "param_name" => "pricing_price",
                            "value" => "",
                            "description" => __("Enter price for this plan.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Pricing plan period:", 'tt_addon_lang'),
                            "param_name" => "pricing_time",
                            "value" => "",
                            "description" => __("Enter your pricing plan period (ex. /month)", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __("Pricing plan currency:", "tt_addon_lang"),
                            "param_name" => "pricing_currency",
                            "value" => array(
                                "Dollar" => "&#36;",
                                "Euro" => "&#128;",
                                "Pound" => "&#163"
                            ),
                            "description" => __("Select pricing plan currency.", "tt_addon_lang")
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Other currency:", 'tt_addon_lang'),
                            "param_name" => "pricing_other_currency",
                            "value" => "",
                            "description" => __("Pricing plan custom currency.", 'tt_addon_lang')
                        ), 
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Pricing subtitle:", 'tt_addon_lang'),
                            "param_name" => "pricing_other_text",
                            "value" => "",
                            "description" => __("Pricing plan subtitle", 'tt_addon_lang')
                        ),                          
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __("Highlight plan", "tt_addon_lang"),
                            "param_name" => "highlight_plan",
                            "value" => array(
                                "No" => "",
                                "Yes" => "active"
                            ),
                            "description" => __("Select if pricing plan is highlighted", "tt_addon_lang")
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Button text:", 'tt_addon_lang'),
                            "param_name" => "pricing_button_text",
                            "value" => "",
                            "description" => __("Pricing table submit button text.", 'tt_addon_lang')
                        ),
						array(
							 "type" => "textfield",
							 "class" => "",
							 "heading" => __("Button link:", "tt_addon_lang"),
							 "param_name" => "pricing_button_link",
							 "value" => "",
							 "description" => __("Set link address and target.", "tt_addon_lang"),
						), 
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 1 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option1_value",
                            "value" => "",
                            "description" => __("Enter your option 1 value.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 2 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option2_value",
                            "value" => ""
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 3 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option3_value",
                            "value" => ""
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 4 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option4_value",
                            "value" => ""
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 5 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option5_value",
                            "value" => ""
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Option 6 value:", 'tt_addon_lang'),
                            "param_name" => "pricing_option6_value",
                            "value" => ""
                        )
                    )
                ));
            }
        }
		
		// Render the element on front-end
        public function tt_pricingtable_shrt($atts, $content = null)
        {
			$output = $link_target = $link_title = $pricing_button_link = $secodary_link = '';
			
            extract(shortcode_atts(array(
                'pricing_title'             => '',
                'pricing_price' 			=> '',
                'pricing_time' 				=> '',
                'pricing_currency' 			=> '',
                'pricing_other_currency'    => '',
                'pricing_other_text' 	    => '',
                'pricing_button_text' 		=> '',
                'pricing_button_link'       => '',
                'highlight_plan' 	      	=> '',
                'pricing_option1_value' 	=> '',
                'pricing_option2_value' 	=> '',
                'pricing_option3_value' 	=> '',
                'pricing_option4_value' 	=> '',
                'pricing_option5_value'     => '',
                'pricing_option6_value' 	=> ''
            ), $atts));
			
            if (!empty($pricing_other_currency)) {
                $pricing_currency = $pricing_other_currency;
            }

            if (($highlight_plan) != "active") {
                $secodary_link = "secondary-button-inverse";
            }
			
			$href = vc_build_link($pricing_button_link);
			
			if($href['url'] !== '') {
				$link_target = (isset($href['target'])) ? 'target="'.$href['target'].'"' : '';
				$link_title = (isset($href['title'])) ? 'title="'.$href['title'].'"' : '';
			}
			
            $output = "
<div class='pricing-table'>
    <div class='row pricing-title'>{$pricing_title}</div>
    <div class='row pricing {$highlight_plan}'>
        <div class='col-lg-3 col-md-3 col-sm-3' >
            <div class='row'>
                <span class='pricing-price'><span class='currency'>{$pricing_currency}</span>{$pricing_price}</span>
                <span class='pricing-time'>/{$pricing_time}</span>
                <div class='billing-time'>{$pricing_other_text}</div>
            </div>
        </div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option1_value}</span></span></div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option2_value}</span></span></div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option3_value}</span></span></div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option4_value}</span></span></div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option5_value}</span></span></div>
        <div class='pricing-row'><span class='pricing-value'><span class='pricing-option'><i class='fa fa-check'></i>{$pricing_option6_value}</span></span></div>
        <div class='pricing-row button-container'>
            <a href='{$pricing_button_link}' class='secondary-button {$secodary_link}'>{$pricing_button_text}</a>
        </div>
    </div>
</div>";
            return $output;
        }
    }
}
if (class_exists('TT_ELEM_PRICING_TABLE')) {
    $TT_ELEM_PRICING_TABLE = new TT_ELEM_PRICING_TABLE;
}
?>
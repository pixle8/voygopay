<?php
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_tek_extended_tabs extends WPBakeryShortCodesContainer {}
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_tek_extended_tabs_single extends WPBakeryShortCode {}
}

if (!class_exists('tek_extended_tabs')) {
    class tek_extended_tabs extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_extended_tabs_init'));
            add_shortcode('tek_extended_tabs', array($this, 'tt_extended_tabs_container'));
            add_shortcode('tek_extended_tabs_single', array( $this, 'tt_extended_tabs_single'));
        }
        // VC Elements render in admin    
        function tt_extended_tabs_init() {
			
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Extended tabs", 'tt_addon_lang'),
                    "description" => __("Vertical tabs with extended features.", 'tt_addon_lang'),
                    "base" => "tek_extended_tabs",
                    "class" => "",
                    "show_settings_on_create" => true,
                    "content_element" => true,
                    "as_parent" => array('only' => 'tek_extended_tabs_single'),
                    "icon" => plugins_url('assets/element_icons/extendedtabs.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "js_view" => 'VcColumnView'
                ));
				
                vc_map(array(
                    "name" => __("Tab", "tt_addon_lang"),
                    "base" => "tek_extended_tabs_single",
                    "content_element" => true,
                    "as_child" => array('only' => 'tek_extended_tabs'),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Title:", "tt_addon_lang"),
                            "param_name" => "services_title",
							"holder" => "tab_title",
							"value" => "",
                            "description" => __("Enter tab title / tab anchor here.")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Subtitle:", "tt_addon_lang"),
                            "param_name" => "services_subtitle",
							"value" => "",
                            "description" => __("Enter tab subtitle here.")
                        ),
                        array(
                            "type" => "exploded_textarea",
                            "heading" => __("Tab Content:", "tt_addon_lang"),
                            "param_name" => "services_content",
                            "value" => "",
                            "description" => __("Content as list ( Enter one feature per line )")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Button:", "tt_addon_lang"),
                            "param_name" => "services_more",
                            "value" => "",
                            "description" => __("Enter tab button text here.")
                        ),
                         array(
                            "type" => "textfield",
                            "heading" => __("Tab Button URL:", "tt_addon_lang"),
                            "param_name" => "services_more_url",
                            "value" => "",
                            "description" => __("Enter tab button url here.")
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __("Tab Image:", "tt_addon_lang"),
                            "param_name" => "services_image",
							"value" => "",
                            "description" => __("You can display one image per tab.")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Image Position - TOP:", "tt_addon_lang"),
                            "param_name" => "services_image_top",
                            "value" => "",
                            "description" => __("Enter Top Image position in px ( eg: 100 or -300 )")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Image Position - RIGHT:", "tt_addon_lang"),
                            "param_name" => "services_image_right",
                            "value" => "",
                            "description" => __("Enter Right Image position in px ( eg: 100 or -300 )")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Image Position - BOTTOM:", "tt_addon_lang"),
                            "param_name" => "services_image_bottom",
                            "value" => "",
                            "description" => __("Enter Bottom Image position in px ( eg: 100 or -300 )")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Tab Image Position - LEFT:", "tt_addon_lang"),
                            "param_name" => "services_image_left",
                            "value" => "",
                            "description" => __("Enter Left Image position in px ( eg: 100 or -300 )")
                        ),
                    )
                ));
            }
        }
        public function tt_extended_tabs_container($atts, $content = null) {
            extract(shortcode_atts(array(), $atts));
            
			$output = "
            <div class='features-tabs'>" . do_shortcode($content) . " 
                <ul class='tabs'></ul>
            </div>";			
            return $output;
        }
		
        public function tt_extended_tabs_single($atts, $content = null) {
            extract(shortcode_atts(array(
                'services_title' 			=> '',
                'services_subtitle'         => '',
                'services_content'			=> '',
                'services_more'             => '',
                'services_more_url'         => '',
                'services_image_top'        => '',
                'services_image_right'      => '',
                'services_image_bottom'     => '',
                'services_image_left' 	    => '',
                'services_image' 			=> ''
            ), $atts));
			
            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $services_image,
                'thumb_size' => 'full',
                'class' => ""
            ));

            $service_title_trim = str_replace(' ', '', $services_title);
            
            $service_content_trim = str_replace(',', '</li><li>', $services_content);
        

            
            $output = "<div id='{$service_title_trim}'>
                            <div class='row'>
                                <h2 class='section-heading'>{$services_title}</h2>
                                <p class='section-subheading '>{$services_subtitle}</p>
                            </div>
                            <ul><li>{$service_content_trim}</li>
                            </ul>
                        <a href='{$services_more_url}' class='primary-button button-inverse'>{$services_more}</a>
                        <div class='tab-image-container' style='margin-top:{$services_image_top}px; margin-right:{$services_image_right}px; margin-bottom:{$services_image_bottom}px; margin-left:{$services_image_left}px;' >{$image['thumbnail']}</div>                     
                        <li class='tab col-md-4'><a href='#{$service_title_trim}'><span class='triangle'><span class='inner-triangle'></span></span>{$services_title}</a></li>
                     </div>";
			
            return $output;
        }
    }
}

if (class_exists('tek_extended_tabs')) {
    $tek_extended_tabs = new tek_extended_tabs;
}
?>
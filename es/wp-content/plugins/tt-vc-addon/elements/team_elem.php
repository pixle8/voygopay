<?php
if (!class_exists('TT_ELEM_TEAM')) {
    class TT_ELEM_TEAM extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_team_init'));
            add_shortcode('tek_team', array($this, 'tt_team_shrt'));
        }
        // Element configuration in admin
        function tt_team_init() {
      
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Team Member", 'tt_addon_lang'),
                    "description" => __("Team member element", 'tt_addon_lang'),
                    "base" => "tek_team",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/team.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(                     
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Team member title:", 'tt_addon_lang'),
                            "param_name" => "title",
                            "value" => "",
                            "description" => __("Enter Team member title.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Team member position:", 'tt_addon_lang'),
                            "param_name" => "position",
                            "value" => "",
                            "description" => __("Enter Team member position.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textarea",
                            "class" => "",
                            "heading" => __("Team member description:", 'tt_addon_lang'),
                            "param_name" => "description",
                            "value" => "",
                            "description" => __("Enter Team member description.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __("Team member image:", "tt_addon_lang"),
                            "param_name" => "image",
                            "description" => __("Upload Team member image.")
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __("Team orientation", "tt_addon_lang"),
                            "param_name" => "orientation",
                            "value" => array(
                                "Up"          => "",
                                "Down"          => "team-member-down"
                            ),
                            "description" => __("Select team member orientation", "tt_addon_lang")
                        ),
                        array(
                            "type" => "colorpicker",
                            "class" => "",
                            "heading" => __("Team member color:", 'tt_addon_lang'),
                            "param_name" => "color",
                            "value" => "",
                            "description" => __("Choose team member color ( default is theme main color )", 'tt_addon_lang')
                        ),   
                        array(
                             "type" => "textfield",
                             "class" => "",
                             "heading" => __("Facebook Link:", "tt_addon_lang"),
                             "param_name" => "facebook_url",
                             "value" => "",
                             "description" => __("Set Facebook address and target.", "tt_addon_lang"),
                        ),
                        array(
                             "type" => "textfield",
                             "class" => "",
                             "heading" => __("Twitter Link:", "tt_addon_lang"),
                             "param_name" => "twitter_url",
                             "value" => "",
                             "description" => __("Set Twitter address and target.", "tt_addon_lang"),
                        ),     
                        array(
                             "type" => "textfield",
                             "class" => "",
                             "heading" => __("Linkedin Link:", "tt_addon_lang"),
                             "param_name" => "linkedin_url",
                             "value" => "",
                             "description" => __("Set Linkedin address and target.", "tt_addon_lang"),
                        )         
                    )
                ));
            }
        }
        
        // Render the element on front-end
        public function tt_team_shrt($atts, $content = null)
        {
            extract(shortcode_atts(array(
                'description'       => '',
                'title'             => '',
                'position'          => '',
                'color'             => '',
                'image'             => '',
                'orientation'       => '',
                'facebook_url'      => '',
                'twitter_url'       => '',
                'linkedin_url'      => ''
            ), $atts));

            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $image,
                'thumb_size' => 'full',
                'class' => ""
            ));

            $output = "<div class='row team-member {$orientation}'>";
            if ($orientation != "team-member-down") { $output .="{$image['thumbnail']}"; }
            $output .=" <div class='team-socials'>
                                <a href='{$facebook_url}' target='_blank'><span class='fa fa-facebook'></span></a>
                                <a href='{$twitter_url}'  target='_blank'><span class='fa fa-twitter'></span></a>
                                <a href='{$linkedin_url}' target='_blank'><span class='fa fa-linkedin'></span></a>
                            </div>
                            <div class='team-content' style='background:{$color}'>
                                <h5>{$title}</h5>
                                <span class='team-subtitle'>{$position}</span>
                                <p>{$description}</p>
                                <span class='triangle' style='border-color:{$color}'></span>
                            </div>";
            if ($orientation == "team-member-down") { $output .="{$image['thumbnail']}"; }
            $output .="</div>";
            return $output;
        }
    }
}
if (class_exists('TT_ELEM_TEAM')) {
    $TT_ELEM_TEAM = new TT_ELEM_TEAM;
}
?>
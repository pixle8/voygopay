/* ------------------------------------------------------------------------
  ADMIN ICON SELECTION
 ------------------------------------------------------------------------*/

function selectIcon(icon) {
        jQuery('.icons').removeAttr('checked');
        jQuery('.icon-admin').removeClass('selected');
        jQuery('.' + icon).addClass('selected');
        jQuery('#icons-' + icon).prop('checked', 'checked');
    }
	
/* ------------------------------------------------------------------------
  ADMIN ICON SEARCH
 ------------------------------------------------------------------------*/

jQuery(function(){
        jQuery(document).on("keyup", ".tt_search_icon", function(){
        	// Retrieve the input field text and reset the count to zero
	escape = function(text) {
	  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
	};
	
	var filter = escape(jQuery(this).val());

	// Loop through the comment list
	jQuery(".icon-admin").each(function(){
		
		// If the list item does not contain the text phrase fade it out
		if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
			jQuery(this).addClass("hidden");
		// Show the list item if the phrase matches and increase the count by 1
		} else {
			jQuery(this).removeClass("hidden");
			
		}
	});  
        });
      });



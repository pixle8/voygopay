<?php
/*
	Plugin Name: KeyDesign VC Add-on - Content elements
	Plugin URI: http://plugins.KeyDesign.com/tt-vc-addon
	Author: KeyDesign
	Author URI: http://www.KeyDesign.ro
	Version: 1.0
	Description: Custom made elements for Visual Composer
	Text Domain: tt_addon_lang
*/

/*
	If accesed directly, exit.
*/
if (!defined('ABSPATH')) die('-1');
if (!class_exists('TT_ADDON_CLASS')) {

	add_action('admin_init','initiate_tt_addon');
	function initiate_tt_addon() {
		/* Verify if Visual Composer is installed and activated */
		$tt_plugin_check = tt_vc_activation();
		if($tt_plugin_check) {
			echo $tt_plugin_check;
		}
	}
	
	/*
		Verify VC version and activation
	*/
	function tt_vc_activation() {
		if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {
		
			/* Visual Composer version check */
			if( version_compare( '4.3.3', WPB_VC_VERSION, '>' ) ) {
				/* Deactivate plug-in if version compare fails */
				if ( is_plugin_active('tt-vc-addon/tt_vc_addon.php') ) {
					deactivate_plugins( '/tt-vc-addon/tt_vc_addon.php', true );
				}
				return show_vc_version_notice();
			}
		} else {
		
			/* Deactivate plug-in if Visual Composer is not installed and active */
			if ( is_plugin_active('tt-vc-addon/tt_vc_addon.php') ) {
				deactivate_plugins( '/tt-vc-addon/tt_vc_addon.php', true );
			}
			return show_vc_version_notice();
		}
		return false;	
	}
	
	/*
		Show notice if plug-in is activated but Visual Composer is not
	*/
	function show_vc_version_notice() {
		echo '
		<div class="updated">
			<p><strong>KeyDesign VC Add-on - Content elements</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a> version 4.3.3</strong> to be installed and activated on your site.</p>
		</div>';
	}
	
	/**
		Load plugin textdomain.
	 */
	add_action( 'plugins_loaded', 'tt_addon_load_textdomain' );
	function tt_addon_load_textdomain() {
		load_plugin_textdomain( 'tt_addon_lang', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' ); 
	}
	
	/**
		Install function
	 */
	register_activation_hook( __FILE__, 'tt_addon_install' );
	function tt_addon_install() {
		update_option('tt_addon_version', '1.0' );
	}
	
	class TT_ADDON_CLASS {
		function __construct() {
			$this->config_folder    = 	plugin_dir_path( __FILE__ ).'config/';
			$this->elements_folder	=	plugin_dir_path( __FILE__ ).'elements/';

			add_action( 'init', array( $this, 'integrate_with_vc' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'tt_load_front_scripts' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'tt_load_back_scripts' ) );
		}

		public function integrate_with_vc() {
			
			/* Include all configuration files */
		    foreach(glob($this->config_folder."/*.php") as $cnfig) {
				require_once($cnfig);
			}   

			/* Include all elements files */
			foreach(glob($this->elements_folder."/*.php") as $elem) {
				require_once($elem);
			}		
		}

		public function tt_load_front_scripts() {

			// Register & Load plug-in main style sheet
			wp_register_style( 'tt_addon_style', plugins_url('assets/css/tt_vc_front.css', __FILE__));
			wp_enqueue_style( 'tt_addon_style' );
			
			// Easing Script
			wp_register_script( 'tt_easing_script', plugins_url('assets/js/jquery.easing.min.js', __FILE__), array('jquery') );
			wp_enqueue_script ( 'tt_easing_script' );

			// OWL Carousel
			wp_register_script( 'tt_carousel_script', plugins_url('assets/js/owl.carousel.min.js', __FILE__), array('jquery') );
			wp_enqueue_script ( 'tt_carousel_script' );

			// Easy Tabs
			wp_register_script( 'tt_easytabs_script', plugins_url('assets/js/jquery.easytabs.min.js', __FILE__), array('jquery') );
			wp_enqueue_script ( 'tt_easytabs_script' );

			// Google Map Script
			wp_register_script( 'tt_google_map', 'https://maps.googleapis.com/maps/api/js?sensor=false', array('jquery') );
			wp_enqueue_script ( 'tt_google_map' );

		    // Custom Google Map
			wp_register_script( 'tt_map_script', plugins_url('assets/js/map.js', __FILE__), array('jquery') );
			wp_enqueue_script ( 'tt_map_script' );

			// Plugin Front End Script
			wp_register_script( 'tt_addon_script', plugins_url('assets/js/tt_vc_script.js', __FILE__), array('jquery') );
			wp_enqueue_script ( 'tt_addon_script' );
			
			
			// Register & Load Font Awesome style
			wp_register_style( 'tt_fa_style', plugins_url('assets/css/font-awesome.min.css', __FILE__));
			wp_enqueue_style( 'tt_fa_style' );
		
		}
		
		public function tt_load_back_scripts() {
			// Register & Load Font Awesome style sheet in back-end
			wp_register_style( 'tt_fa_back', plugins_url('assets/css/font-awesome.min.css', __FILE__));
			wp_enqueue_style( 'tt_fa_back' );

			// Register & Load plug-in main style sheet in back-end
			wp_register_style( 'tt_back_style', plugins_url('assets/css/tt_vc_back.css', __FILE__));
			wp_enqueue_style( 'tt_back_style' );
			
			// Register & Load icon search module script
			wp_register_script( 'tt_addon_icon_selector', plugins_url('assets/js/icon-selector.js', __FILE__));
			wp_enqueue_script ( 'tt_addon_icon_selector' );
		}

	}
}
// Finally initialize code
new TT_ADDON_CLASS();
/*
---

script: claims.js
description: JS to handle form validation in claims section.
copyright: (c) 2015 NovoPayment, Inc.

...
*/


	window.addEvent('domready', function() {

		var form = document.id('content').getElement('form');
		var fields = form.getElements('input[type!="hidden"], select, textarea');
		var constants = form.getElements('input[type="hidden"]');
		var buttons = form.getElements('button');
		var message = document.id('message');

		


		if (fields.length > 0) {
			form.addEvent('submit', function(e) {
				e.preventDefault();
				send();
			});

			buttons.filter(function(button) {
				return button.get('type') == 'submit';
			}).each(function(button) {
				button.altText = button.get('text');
				button.removeProperty('disabled');
			});

			fields.filter(function(field) {
				return field.get('tag') == 'select';
			}).each(function(field) {
				field.addEvent('change', function(e) {
					if (typeOf(document.id(e.target.get('data-child'))) == 'element') {
						var child = document.id(e.target.get('data-child'));
						if (e.target.selectedIndex <= 0) {
							if (!child.get('disabled')) {
								child.set('disabled', true);
							}

							clearOptions(child);

						} else {
							if (child.get('disabled')) {
								child.removeProperty('disabled');
							}

							var code = (typeOf(e.target.options[e.target.selectedIndex].get('data-code')) == 'string') ? e.target.options[e.target.selectedIndex].get('data-code') : null;

							getOptions(child, code);

						}
					}
				});

				field.addEvent('keyup', function(e) {
					this.fireEvent('change', e);
				});
			});

			window.addEvent('load', function() {
				['id-type', 'region', 'product-name', 'cause'].each(function(field) {
					getOptions(document.id(field));
				});
			});

			var getOptions = function(field, code) {
				var reqData = {
					'type': field.get('data-type'),
					'kind': field.get('id')
				};

				if (typeOf(code) == 'string') {
					reqData.code = code;
				}

				new Request.JSON({
					url: '/claims-data/',
					data: reqData,
					method: 'post',
					onSuccess: function(response) {
						var results = response.results;

						clearOptions(field);

						if (results.length > 0) {
							new Element('option', {
								'text': '— Seleccione —',
								'value': '',
								'selected': true
							}).inject(field);

							results.each(function(opt) {
								if (typeOf(opt.nodes) == 'array' && opt.nodes.length > 0) {
									var optgroup = new Element('optgroup', {
										'data-value': opt.name,
										'label': (typeOf(opt.nice_name) == 'string' ? opt.nice_name : opt.name),
									}).inject(field);

									opt.nodes.each(function(node) {
										new Element('option', {
											'text': (typeOf(node.nice_name) == 'string' ? node.nice_name : node.name),
											'value': node.name
										}).inject(optgroup);
									});

								} else {
									var option = new Element('option', {
										'text': (typeOf(opt.nice_name) == 'string' ? opt.nice_name : opt.name),
										'value': opt.name
									}).inject(field);

									if (typeOf(opt.code) == 'string') {
										option.set('data-code', opt.code);
									}

								}
							});
						} else {
							console.log(response.error);
						}
					},
					onError: function(text, error) {
						return false;
					}
				}).send();
			}

			var clearOptions = function(field) {
				field.erase('html');
			}

			var send = function() {
				var errors = [];

				if (typeOf(message) !== 'null') {
					message.destroy();
				}

				form.getElements('.error').each(function(el) {
					el.removeClass('error');
				});

				fields.each(function(field) {
					field.isValid = true;

					if (!field.get('disabled') && field.isValid) {
						if (field.match('[data-validate~="required"]')) {
							if (field.get('tag') !== 'select' && !field.get('value').test(/[^.*]/)) {
								field.isValid = false;
								errors.include('Debe especificar <strong>' + field.get('title') + '</strong>.');
							} else if (field.get('tag') == 'select' && field.selectedIndex <= 0) {
								field.isValid = false;
								errors.include('Debe seleccionar <strong>' + field.get('title') + '</strong>.');
							}
						}

						if (field.get('value').length > 0 && field.isValid) {
							if (field.match('[data-validate~="alpha"]') && !field.get('value').test(/^[a-zA-Z_-]$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> solamente permite caracteres alfabéticos.');
							}

							if (field.match('[data-validate~="alphanum"]') && !field.get('value').test(/[a-zA-Z0-9_-]$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> solamente permite caracteres alfanuméricos.');
							}

							if (field.match('[data-validate~="int"]') && !field.get('value').test(/^\d+$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> debe ser un número entero válido.');
							}

							if (field.match('[data-validate~="currency"]') && !field.get('value').test(/^\d*(\.|\,)?\d+$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> debe ser una cantidad numérica válida.');
							}

							if (field.match('[data-validate~="email"]') && !field.get('value').test(/^([a-zA-Z0-9_\.\-\+%])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> debe ser válido.');
							}

							if (field.match('[data-validate~="cc-num"]') && !field.get('value').test(/^\d{16}$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> debe contener los 16 digitos.');
							}

							if (field.match('[data-validate~="nospaces"]') && !field.get('value').test(/^[A-Z0-9a-z]*$/)) {
								field.isValid = false;
								errors.include('<strong>' + field.get('title') + '</strong> no admite espacios ni carácteres especiales.');
							}
						}

						if (!field.isValid)
							field.addClass('error');
					}
				});

				if (errors.length > 0) {
					buttons.filter(function(button) {
						return button.get('type') == 'submit';
					}).each(function(button) {
						button.removeProperty('disabled');
						button.set('text', button.altText);
					});

					message = new Element('div', {
						'class': 'alert-box failure',
						id: 'errors'
					}).inject(form, 'top');

					new Element('p', {
						text: 'Por favor, verifique y corrija los siguientes errores detectados:'
					}).inject(message);

					var errorList = new Element('ul').inject(message);

					errors.each(function(el) {
						new Element('li', {
							html: el
						}).inject(errorList);
					});

					var coords = message.getPosition();
					window.scrollTo(coords.x, coords.y);
				} else {
					fields.filter(function(field) {
						return (field.get('tag') == 'select' && field.getChildren('optgroup').length > 0);
					}).each(function(field) {
						var metaFields = field.getAllNext('input[type="hidden"]');

						if (typeOf(field.getSelected().getParent('optgroup')) == 'elements') {
							metaFields[0].removeProperty('value').set('value', field.getSelected().getParent('optgroup')[0].get('data-value'));
							metaFields[1].removeProperty('value').set('value', field.get('value'));
						} else {
							metaFields[0].removeProperty('value').set('value', field.get('value'));
							metaFields[1].removeProperty('value');
						}
					});

					fields.filter(function(field) {
						return field.match('[data-validate~="cc-num"]');
					}).each(function(field) {
						var target = form.getElement('textarea');
						var metaField = target.getNext('input[type="hidden"]');

						metaField.removeProperty('value').set('value', field.get('value') + ' // ' + target.get('value'));
					});

					fields.filter(function(field) {
						return (field.match('[data-validate~="currency"]') && field.get('value').test(/[^.*]/));
					}).each(function(field) {
						var metaField = field.getNext('input[type="hidden"]');

						metaField.removeProperty('value').set('value', field.get('value').replace(/\,/g, '.'));
					});

					form.submit();
				}
			}
		}



	});

// IIFE - Immediately Invoked Function Expression
(function (main_js) {

    // The global jQuery object is passed as a parameter
    main_js(window.jQuery, window, document);

}(function ($, window, document) {

    // The $ is now locally scoped

    // Listen for the jQuery ready event on the document
    $(function () {
        // The DOM is ready!
       
       setTimeout(function(){ $('#product-name').val('Provis Alimentación'); }, 2000);
		
    });

    // The rest of the code goes here!
}));

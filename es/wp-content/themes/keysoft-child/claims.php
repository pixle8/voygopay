<?php

// Template Name: Claims 

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'claims.php' == basename($_SERVER['SCRIPT_FILENAME'])) die('Por favor, no cargue este archivo directamente.');

get_header(); ?>
<div id="wrapper">
	<div id="left-sidebar">
		<?php if (function_exists('novo_services')) novo_services(); ?>
	</div>
	<div id="content">
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
		<div id="content-title">
			<img src="<?php bloginfo('template_directory'); ?>/images/icons/content_default.jpg" width="56" height="56" alt="" />
			<h2><?php the_title(); ?></h2>
			<?php $subtitle = get_post_meta($post->ID, 'subtitle', true);
			if ($subtitle): ?><h3><?php echo $subtitle; ?></h3><?php endif; ?>
		</div>
		<div id="content-wrap">
			<?php
			$process = !empty($_GET['process']) ? strtolower($_GET['process']) : 'none';
			
			if ($process === 'none'):
				the_content(); ?>
			<form accept-charset="utf-8" action="https://www.salesforce.com/servlet/servlet.WebToCase?encoding=UTF-8" method="post" autocomplete="off">
				<input type="hidden" name="orgid" value="00Do0000000e0vQ" />
				<input type="hidden" name="retURL" value="<?php echo get_permalink(get_page_by_path('reclamos')) . '?process=success'; ?>" />
				<fieldset>
					<legend>Información de Afiliación</legend>
					<input type="hidden" name="recordType" value="012o0000000xR5t" />
					<label>Nombre Completo
						<input class="field-full" data-validate="required" type="text" id="full-name" name="00No000000CLkN1" maxlength="80" title="Nombre Completo o Razón Social" />
					</label>
					<label>Documento de Identidad
						<ul class="field-group">
							<li>
								<select class="field-md" data-type="global" data-validate="required" id="id-type" name="00No000000C8ZKz" title="Tipo de Documento de Identidad"></select>
							</li>
							<li>
								<input class="field-md" data-validate="required" type="text" id="id-number" name="00No000000C6kUJ" maxlength="30" title="Documento de Identidad" />
							</li>
						</ul>
					</label>
					<ul class="field-group">
						<li>
							<label>Departamento
								<select class="field-md" data-type="ubigeo" data-child="province" data-validate="required" id="region" name="00No000000C6kUO" title="Departamento"></select>
							</label>
						</li>
						<li>
							<label>Provincia
								<select class="field-md" data-type="ubigeo" data-child="district" data-validate="required" id="province" name="00No000000C6kUZ" title="Provincia" disabled></select>
							</label>
						</li>
					</ul>
					<label>Distrito
						<select class="field-full" data-type="ubigeo" data-validate="required" id="district" name="00No000000C6kUP" title="Distrito" disabled></select>
					</label>
					<label>Correo Electrónico
						<input class="field-full" data-validate="required email" type="text" id="email" name="email" maxlength="80" title="Correo Electrónico" />
					</label>
					<label>Teléfono
						<input class="field-md" data-validate="required" type="text" id="phone" name="phone" maxlength="40" title="Teléfono" />
					</label>
				</fieldset>
				<fieldset>
					<legend>Información de Reclamo</legend>
					<label>Tipo de Producto
						<select class="field-lg" data-type="global" data-validate="required" id="product-name" name="00No000000C8WbE" title="Tipo de Producto"></select>
					</label>
					<label>Moneda</label>
					<input type="radio" id="currency-pen" name="currency" value="PEN" checked />
					<label for="currency-pen">PEN - Soles</label>
					<input type="radio" id="currency-usd" name="currency" value="USD" />
					<label for="currency-usd">USD - Dólares</label>
					<label>Número de Tarjeta
						<input class="field-md" data-validate="required cc-num" type="text" id="product-number" maxlength="16" title="Número de Tarjeta" />
					</label>
					<input type="hidden" name="type" value="Reclamo" />
					<input type="hidden" name="origin" value="03 Web" />
					<input type="hidden" name="priority" value="WebToCase" />
					<input type="hidden" name="00No000000C8n51" value="De 1 a 15 días" />
					<label>Motivo
						<select class="field-full" data-type="global" data-validate="required" id="cause" title="Motivo"></select>
						<input type="hidden" name="00No000000C6kUU" />
						<input type="hidden" name="00No000000CH9Gp" />
					</label>
					<label>Monto <span class="complimentary">(opcional)</span>
						<input class="field-md" data-validate="currency" type="text" id="amount" maxlength="20" title="Monto" />
						<input type="hidden" name="00No000000C8Zr0" />
					</label>
					<label>Asunto
						<input class="field-full" data-validate="required alphanum" type="text" id="subject" name="subject" maxlength="80" placeholder="Defina un título para su problema" title="Asunto" />
					</label>
					<label>Descripción
						<textarea class="field-full" data-validate="required" id="description" placeholder="Describa su reclamo en detalle" title="Descripción"></textarea>
						<input type="hidden" name="description" />
					</label>
					<input type="hidden" name="status" value="En Espera" />
				</fieldset>
				<input type="hidden" name="external" value="1" />
				<button type="reset">Cancelar</button>
				<button type="submit" disabled>Enviar</button>
			</form>
			<?php elseif ($process === 'success'): ?>
			<form accept-charset="utf-8" action="<?php bloginfo('home'); ?>" method="get" autocomplete="off">
				<div class="alert-box success">
					<p>Su reclamo ha sido recibido por nuestro equipo de Servicio al Cliente.</p>
					<p>A fin de concretar la presentación de su solicitud es necesario hacer la validación correspondiente a los datos recibidos. Nos estaremos comunicando con Ud. al correo electrónico indicado, agradecemos estar atento a nuestra respuesta.</p>
					<p>Cualquier consulta adicional, puede llamar a nuestras <a href="<?php echo get_permalink(get_page_by_path('contacto')); ?>" title="Contáctenos">Líneas de Atención</a> donde será atendido por nuestros operadores de servicio.</p>
					<button type="submit">Continuar</button>
				</div>
			</form>
			<?php else: ?>
			<form accept-charset="utf-8" action="<?php echo get_permalink(get_page_by_path('reclamos')); ?>" method="get" autocomplete="off">
				<div class="alert-box warning">
					<p>Ha ocurrido un error grave procesando su reclamo, por tanto la información correspondiente al caso no fue enviada a nuestro personal encargado.</p>
					<p>Por favor, vuelva a rellenar el formulario correctamente.</p>
					<p>Disculpe los inconvenientes.</p>
					<button type="submit">Reintentar</button>
				</div>
			</form>
			<?php endif; ?>
		</div>
		<div id="content-nav"></div>
		<?php endwhile; endif; ?>
	</div>
	<div id="right-sidebar">
		<?php if (function_exists('novo_banners')) novo_banners(2); ?>
	</div>
	<?php $zone_id = $post->post_parent ? $post->post_parent : $post->ID;
	if (function_exists('novo_tidbits')) novo_tidbits($zone_id); ?>
</div>
<?php get_footer(); ?>

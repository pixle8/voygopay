<?php
/**
 * @package WordPress
 * @subpackage Provis
 * @since 1.1.3
 */

// Include WordPress configuration to start application
require_once('wp-config.php');

// Start needed WordPress objects
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();

// Parse data received as request and process accordingly
$data = array();
$type = $_POST['type'];
$kind = !empty($_POST['kind']) ? $_POST['kind'] : null;
$code = !empty($_POST['code']) ? $_POST['code'] : null;

// Get results from MySQL database under WordPress schema
if ($type === 'global') {
	$sql = $wpdb->prepare("SELECT option_id, option_name, option_nice_name FROM options_global WHERE option_kind = %s AND option_parent IS NULL", $kind);
	$results = $wpdb->get_results($sql);

	if ($results) {
		foreach ($results as $result) {
			$nodes_sql = $wpdb->prepare("SELECT option_name AS name, option_nice_name AS nice_name FROM options_global WHERE option_kind = %s AND option_parent = %d", $kind, $result->option_id);
			$nodes = $wpdb->get_results($nodes_sql);

			$data['results'][] = array(
				'name' => $result->option_name,
				'nice_name' => $result->option_nice_name,
				'nodes' => $nodes
			);
		}

	} else {
		$data['error'] = array(
			'code' => 'no_results',
			'message' => 'No results were found.'
		);

	}

} elseif ($type === 'ubigeo') {
	$sql = null;
	
	if (!is_null($code)) {
		if ($kind === 'district') {
			$regex = '^(' . substr($code, 0, 4) . ')[0-9]{2}$';
			$regex_deny = '0{2}$';
			
		} else {
			$regex = '^(' . substr($code, 0, 2) . ')[0-9]{2}0{2}$';
			$regex_deny = '0{4}$';
			
		}
		$sql = $wpdb->prepare("SELECT ubigeo_code, ubigeo_name FROM options_ubigeo WHERE ubigeo_code REGEXP %s AND ubigeo_code NOT REGEXP %s ORDER BY ubigeo_name ASC", $regex, $regex_deny);
		
	} else {
		$regex = '^[0-9]{2}0{4}$';
		$sql = $wpdb->prepare("SELECT ubigeo_code, ubigeo_name FROM options_ubigeo WHERE ubigeo_code REGEXP %s ORDER BY ubigeo_name ASC", $regex);
		
	}
	$results = $wpdb->get_results($sql);
	
	if ($results) {
		foreach ($results as $result) {
			$data['results'][] = array(
				'code' => $result->ubigeo_code,
				'name' => $result->ubigeo_name
			);
		}
		
	} else {
		$data['error'] = array(
			'code' => 'no_results',
			'message' => 'No results were found.'
		);
		
	}
	
} else {
	$data['error'] = array(
		'code' => 'type_undefined',
		'message' => 'Type was not defined.'
	);
	
}

// Output data object as JSON
print json_encode($data);
?>

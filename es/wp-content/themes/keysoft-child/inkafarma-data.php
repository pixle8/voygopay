<?php
/**
 * @package WordPress
 * @subpackage Provis
 * @since 1.0.0
 */

// Include WordPress configuration to start application
require_once('wp-config.php');

// Start needed WordPress objects
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();

// Parse data received as request and process accordingly
$data = array();
$app_key = $_POST['key'];


	$data['key'] = $app_key;
	
	// Store data variables
	$region = !empty($_POST['region']) ? $_POST['region'] : null;
	$district = !empty($_POST['district']) ? $_POST['district'] : null;
	
	// Get results from MySQL database under WordPress
	if (is_null($region)) {
		$results = $wpdb->get_results("SELECT DISTINCT(TRIM(pharmacy_region)) AS pharmacy_region FROM provis_pharmacies");
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->pharmacy_region;
		} else {
			$data['issue'] = 'noResults';
		}
	} elseif (!is_null($region) && is_null($district)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(pharmacy_district)) AS pharmacy_district FROM provis_pharmacies WHERE pharmacy_region = %s ORDER BY pharmacy_district", $region);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->pharmacy_district;
		} else {
			$data['issue'] = 'noResults';
		}
	} elseif (!is_null($region) && !is_null($district)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(pharmacy_name)) AS pharmacy_name, TRIM(pharmacy_address) AS pharmacy_address, TRIM(pharmacy_province) as pharmacy_province FROM provis_pharmacies WHERE pharmacy_region = %s AND pharmacy_district = %s ORDER BY pharmacy_name", $region, $district);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = array(
					'name' => $result->pharmacy_name,
					'address' => $result->pharmacy_address,
					'province' => $result->pharmacy_province
				);
		} else {
			$data['issue'] = 'noResults';
		}
	} else {
		$data['issue'] = 'offline';
	}

	
print json_encode($data);
?>

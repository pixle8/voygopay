(function($) {
	$(document).ready(function() {
		var pathname = window.location.pathname;
		console.log('Pathname is ' + pathname);
		
		if(pathname == '/sobre-voygo/') {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com/about-voygo");
		} else if (pathname == '/como-funciona/') {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com/how-it-works");
		} else if (pathname == '/beneficios/') {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com/benefits");
		} else if (pathname == '/clientes/') {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com/clients");
		} else if (pathname == '/contactenos/') {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com/contact-us");
		} else {
			$("#menu-item-1861 > a").attr("href", "http://voygo.com");
		}
	});
})(jQuery);
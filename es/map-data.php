<?php
/**
 * @package WordPress
 * @subpackage Provis
 * @since 1.0.0
 */

// Include WordPress configuration to start application
require_once('wp-config.php');

// Start needed WordPress objects
$wp->init();
$wp->parse_request();
$wp->query_posts();
$wp->register_globals();
$wp->send_headers();

// Parse data received as request and process accordingly
$data = array();
$app_key = $_POST['key'];

if (strcmp($app_key, get_option('np_provis_map_key')) == 0) {
	$data['key'] = $app_key;
	
	// Store data variables
	$brand = !empty($_POST['brand']) ? $_POST['brand'] : null;
	$region = !empty($_POST['region']) ? $_POST['region'] : null;
	$district = !empty($_POST['district']) ? $_POST['district'] : null;
	$category = !empty($_POST['category']) ? $_POST['category'] : null;
	
	// Get results from MySQL database under WordPress
	if (is_null($brand)) {
		$results = $wpdb->get_results("SELECT DISTINCT(TRIM(commerce_brand)) AS commerce_brand FROM {$wpdb->prefix}commerces");
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->commerce_brand;
		} else {
			$data['issue'] = 'offline';
		}
	} elseif (!is_null($brand) && is_null($region)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(commerce_region)) AS commerce_region FROM {$wpdb->prefix}commerces WHERE commerce_brand = %s", $brand);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->commerce_region;
		} else {
			$data['issue'] = 'noResults';
		}
	} elseif (!is_null($brand) && !is_null($region) && is_null($district)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(commerce_district)) AS commerce_district FROM {$wpdb->prefix}commerces WHERE commerce_brand = %s AND commerce_region = %s ORDER BY commerce_district", $brand, $region);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->commerce_district;
		} else {
			$data['issue'] = 'noResults';
		}
	} elseif (!is_null($brand) && !is_null($region) && !is_null($district) && is_null($category)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(commerce_category)) AS commerce_category FROM {$wpdb->prefix}commerces WHERE commerce_brand = %s AND commerce_region = %s AND commerce_district = %s ORDER BY commerce_category", $brand, $region, $district);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = $result->commerce_category;
		} else {
			$data['issue'] = 'noResults';
		}
	} elseif (!is_null($brand) && !is_null($region) && !is_null($district) && !is_null($category)) {
		$sql = $wpdb->prepare("SELECT DISTINCT(TRIM(commerce_name)) AS commerce_name, TRIM(commerce_address) AS commerce_address, TRIM(commerce_province) as commerce_province FROM {$wpdb->prefix}commerces WHERE commerce_brand = %s AND commerce_region = %s AND commerce_district = %s AND commerce_category = %s ORDER BY commerce_name", $brand, $region, $district, $category);
		$results = $wpdb->get_results($sql);
		if ($results) {
			foreach ($results as $result)
				$data['results'][] = array(
					'name' => $result->commerce_name,
					'address' => $result->commerce_address,
					'province' => $result->commerce_province
				);
		} else {
			$data['issue'] = 'noResults';
		}
	} else {
		$data['issue'] = 'offline';
	}
} else {
	$data['issue'] = 'unauthorized';
}
	
print json_encode($data);

?>

(function($) {
	$(document).ready(function() {
		var pathname = window.location.pathname;
		console.log('Pathname is ' + pathname);
		
		if(pathname == '/about-voygo/') {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com/sobre-voygo");
		} else if (pathname == '/how-it-works/') {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com/como-funciona");
		} else if (pathname == '/benefits/') {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com/beneficios");
		} else if (pathname == '/clients/') {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com/clientes");
		} else if (pathname == '/contact-us/') {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com/contactenos");
		} else {
			$("#menu-item-1854 > a").attr("href", "http://es.voygo.com");
		}
	});
})(jQuery);
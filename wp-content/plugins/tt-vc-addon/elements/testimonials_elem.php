<?php
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_tek_testimonials extends WPBakeryShortCodesContainer {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_tek_testimonials_single extends WPBakeryShortCode {
    }
}
if (!class_exists('tek_testimonials')) {
    class tek_testimonials extends TT_ADDON_CLASS
    {
        function __construct() {
            add_action('admin_init', array($this, 'tt_testimonials_init'));
            add_shortcode('tek_testimonials', array($this, 'tt_testimonials_container'));
            add_shortcode('tek_testimonials_single', array($this, 'tt_testimonials_single'));
        }
        // Element configuration in admin   
        function tt_testimonials_init() {
            // Container element configuration
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Testimonials", 'tt_addon_lang'),
                    "description" => __("Sliding testimonials with author image.", 'tt_addon_lang'),
                    "base" => "tek_testimonials",
                    "class" => "",
                    "show_settings_on_create" => true,
                    "content_element" => true,
                    "as_parent" => array('only' => 'tek_testimonials_single'),
                    "icon" => plugins_url('assets/element_icons/testimonials.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(                   
                        array(
                            "type" => "colorpicker",
                            "class" => "",
                            "heading" => __("Testimonial text color:", 'tt_addon_lang'),
                            "param_name" => "tt_text_color",
                            "value" => '',
                            "description" => __("Choose testimonial text color.", 'tt_addon_lang')
                        )
                    ),
                    "js_view" => 'VcColumnView'
                ));
                // Shortcode configuration
                vc_map(array(
                    "name" => __("Testimonial", "tt_addon_lang"),
                    "base" => "tek_testimonials_single",
                    "content_element" => true,
                    "as_child" => array('only' => 'tek_testimonials'),
                    "icon" => plugins_url('assets/element_icons/testimonials.png', dirname(__FILE__)),
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "heading" => __("Author quote:", "tt_addon_lang"),
                            "holder" => "author_quote",
                            "param_name" => "tt_quote",
                            "description" => __("Testimonial author quote.")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Author name:", "tt_addon_lang"),
                            "holder" => "author_name",
                            "param_name" => "tt_title",
                            "description" => __("Testimonial author name.")
                        ),
                        array(
                            "type" => "textfield",
                            "heading" => __("Author position:", "tt_addon_lang"),
                            "param_name" => "tt_position",
                            "description" => __("Testimonial author position in company.")
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __("Author image:", "tt_addon_lang"),
                            "param_name" => "tt_image",
                            "description" => __("Display testimonial author image.")
                        )
                    )
                ));
            }
        }
        
        public function tt_testimonials_container($atts, $content = null) {
            extract(shortcode_atts(array(
                'tt_text_color' => ''
              ), $atts));
            $output = "
            <div class='slider testimonials'>" . do_shortcode($content) . "</div>
            <div class='tt-images'></div>";
            return $output;
        }
        
        public function tt_testimonials_single($atts, $content = null) {
            extract(shortcode_atts(array(
                'tt_title'          => '',
                'tt_quote'          => '',
                'tt_position'       => '',
                'tt_image'          => ''
            ), $atts));
            
            $image  = wpb_getImageBySize($params = array(
                'post_id' => NULL,
                'attach_id' => $tt_image,
                'thumb_size' => 'full',
                'class' => ""
            ));
            
            $output = "<div class='tt-content'>
                            <h3><span class='tt-quote'>“</span><span class='tt-quote tt-quote-right'>”</span>{$tt_quote}</h3>
                            <div class='tt-container'>
                                <h4 >{$tt_title}</h4>
                                <span class='content'>{$tt_position}</span>
                            </div>
                            <div class='tt-image'>{$image['thumbnail']}</div>
                        </div>";
            return $output;
        }
    }
}
if (class_exists('tek_testimonials')) {
    $tek_testimonials = new tek_testimonials;
}
?>
<?php
if (!class_exists('TT_ELEM_ICON_BOX')) {
    class TT_ELEM_ICON_BOX extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_iconbox_init'));
            add_shortcode('tek_iconbox', array($this, 'tt_iconbox_shrt'));
        }
        // Element configuration in admin
        function tt_iconbox_init() {
            global $icons;
            include(plugin_dir_path(dirname(__FILE__)) . 'config/icon-selector.php');
      
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Icon Box", 'tt_addon_lang'),
                    "description" => __("Simple text box with icon.", 'tt_addon_lang'),
                    "base" => "tek_iconbox",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/iconbox.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(

                        array(
                            "type" => "textfield",
                            "class" => "tt-search-icon",                           
                            "heading" => __("Search icon:", 'tt_addon_lang'),
                            "param_name" => "tt_search_icon",
                            "value" => "",
							"description" => __("You can find icons easily with this search bar.", 'tt_addon_lang')
                         ),
                        array(
                            "type" => "checkbox",
                            "class" => "",
                            "heading" => __("Icons:", 'tt_addon_lang'),
                            "param_name" => "icons",
                            "value" => $icons
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
							"holder" => "iconbox_title",
                            "heading" => __("Box title:", 'tt_addon_lang'),
                            "param_name" => "title",
                            "value" => "",
                            "description" => __("Enter box title here.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textarea",
                            "class" => "",
                            "heading" => __("Box content text:", 'tt_addon_lang'),
                            "param_name" => "text_box",
                            "value" => "",
                            "description" => __("Enter box content text here.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "colorpicker",
                            "class" => "",
                            "heading" => __("Icon color:", 'tt_addon_lang'),
                            "param_name" => "color",
                            "value" => "",
                            "description" => __("Choose icon color ( default is theme main color )", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __("Icon/Text Position", "tt_addon_lang"),
                            "param_name" => "icon_position",
                            "value" => array(
                                "Left"          => "left",
                                "Right"         => "right",
                                "Center"        => "center"
                            ),
                            "description" => __("Select icon/text position.", "tt_addon_lang")
                        )          
                    )
                ));
            }
        }
		
		// Render the element on front-end
        public function tt_iconbox_shrt($atts, $content = null)
        {
            extract(shortcode_atts(array(
                'icons' 			=> '',
                'color' 			=> '',
                'text_box'          => '',
                'icon_position' 	=> '',
                'title' 			=> ''
            ), $atts));

            $output = "
                <div class='key-icon-box' style='text-align:{$icon_position}'> 
                <i class='{$icons} fa' style='color:{$color}'></i>
                <h4 class='service-heading'>{$title}</h4>
                <p>{$text_box}</p>
                </div>";
            return $output;
        }
    }
}
if (class_exists('TT_ELEM_ICON_BOX')) {
    $TT_ELEM_ICON_BOX = new TT_ELEM_ICON_BOX;
}
?>
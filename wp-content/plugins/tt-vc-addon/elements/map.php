<?php
if (!class_exists('TT_ELEM_MAP')) {
    class TT_ELEM_MAP extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_map_init'));
            add_shortcode('tek_map', array($this, 'tt_map_shrt'));
        }
        // Element configuration in admin       
        function tt_map_init() {
            
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Google Map", 'tt_addon_lang'),
                    "description" => __("Custom Google Map", 'tt_addon_lang'),
                    "base" => "tek_map",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/pie.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Location Latitude:", 'tt_addon_lang'),
                            "param_name" => "map_latitude",
                            "value" => "",
                            "description" => __("Enter location latitude.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Location Longitude:", 'tt_addon_lang'),
                            "param_name" => "map_longitude",
                            "value" => "",
                            "description" => __("Enter location longitude.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Map Zoom:", 'tt_addon_lang'),
                            "param_name" => "map_zoom",
                            "value" => "",
                            "description" => __("Enter map zoom ( default 13 )", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "attach_image",
                            "heading" => __("Map marker icon:", "tt_addon_lang"),
                            "param_name" => "map_icon",
                            "description" => __("Upload Map marker icon")
                        )
                    )
                ));
            }
        }
        
        // Render the element on front-end
        public function tt_map_shrt($atts, $content = null) {
            extract(shortcode_atts(array(
                'map_latitude'        => '',
                'map_zoom'        => '',
                'map_icon'        => '',
                'map_longitude'        => ''

            ), $atts));


        $img = wp_get_attachment_image_src($map_icon, "large");
        $imgSrc = $img[0];
            
            $output = "
            <div class='contact-map-container'>
                <div id='map' data-latitude={$map_latitude} data-longitude={$map_longitude} data-zoom={$map_zoom} data-icon='{$imgSrc}'></div>
            </div>";
            return $output;
        }
    }
}

if (class_exists('TT_ELEM_MAP')) {
    $TT_ELEM_MAP = new TT_ELEM_MAP;
}
?>
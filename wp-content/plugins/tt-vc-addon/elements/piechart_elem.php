<?php
if (!class_exists('TT_ELEM_PIE_CHART')) {
    class TT_ELEM_PIE_CHART extends TT_ADDON_CLASS {
        function __construct() {
            add_action('admin_init', array($this, 'tt_piechart_init'));
            add_shortcode('tek_pie', array($this, 'tt_piechart_shrt'));
        }
        // Element configuration in admin		
        function tt_piechart_init() {
            global $icons;
            include(plugin_dir_path(dirname(__FILE__)) . 'config/icon-selector.php');
			
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name" => __("Pie Chart", 'tt_addon_lang'),
                    "description" => __("Flipping animated pie chart.", 'tt_addon_lang'),
                    "base" => "tek_pie",
                    "class" => "",
                    "icon" => plugins_url('assets/element_icons/pie.png', dirname(__FILE__)),
                    "category" => "KeyDesign Elements",
                    "params" => array(
                        array(
                            "type" => "textfield",
                            "class" => "",
                            "heading" => __("Pie chart value:", 'tt_addon_lang'),
                            "param_name" => "pie_number",
                            "value" => "",
                            "description" => __("Enter a number value.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "textfield",
                            "class" => "",
							"holder" => "piechart_text",
                            "heading" => __("Pie chart text:", 'tt_addon_lang'),
                            "param_name" => "pie_text",
                            "value" => "",
                            "description" => __("Enter your description here.", 'tt_addon_lang')
                        ),
                        array(
                            "type" => "checkbox",
                            "class" => "",
                            "heading" => __("Icons:", 'tt_addon_lang'),
                            "param_name" => "icons",
                            "value" => $icons
                        ),
                        array(
                            "type" => "dropdown",
                            "class" => "",
                            "heading" => __("Display percent value:", "tt_addon_lang"),
                            "param_name" => "pie_percent",
                            "value" => array(
                                "Yes" 	=> "",
                                "No" 	=> "hide-percent"
                            ),
                            "description" => __("Display percent symbol after number.", "tt_addon_lang")
                        ),
                        array(
                            "type" => "colorpicker",
                            "class" => "",
                            "heading" => __("Color:", 'tt_addon_lang'),
                            "param_name" => "chart_color",
                            "value" => "",
                            "description" => __("Choose color.", 'tt_addon_lang')
                        ),
                    )
                ));
            }
        }
		
		// Render the element on front-end
        public function tt_piechart_shrt($atts, $content = null) {
            extract(shortcode_atts(array(
                'pie_number' 		=> '',
                'pie_text' 			=> '',
                'pie_size' 			=> '',
                'icons'             => '',
                'pie_percent'       => '',
                'chart_color' 		=> '',
            ), $atts));
			
            $output = " <div class='chart' style='color:{$chart_color}' data-bar-color='{$chart_color}' data-percent='{$pie_number}'>{$pie_number}<span class='percent {$pie_percent}'>%</span> <canvas height='230' width='230'></canvas></div>
                        <div class='chart-content'>
                            <i style='color:{$chart_color}' class='{$icons} fa'></i>
                            <h4 class='service-heading'>{$pie_text}</h4>
                        </div>";
            return $output;
        }
    }
}

if (class_exists('TT_ELEM_PIE_CHART')) {
    $TT_ELEM_PIE_CHART = new TT_ELEM_PIE_CHART;
}
?>